<?php

namespace App\Http\Controllers\Admin;

use App\Models\Costumer;
use App\Models\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CostumerStore;
use App\Http\Requests\Admin\CostumerUpdate;
use Exception;
use Illuminate\Http\Request;

class CostumerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $costumer = Costumer::paginate(5);

        return view('admin.costumer', ['costumer' => $costumer]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\CostumerStore  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CostumerStore $request)
    {
        Costumer::create($request->all());
        return back()->with('success', 'Costumer Inserted');;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Costumer  $costumer
     * @return \Illuminate\Http\Response
     */
    public function show(Costumer $costumer,Request $request)
    {
        $costumer = $costumer->products()->paginate(5);

        $view = view('item.row-price',['costumer' => $costumer])->render();
        
        if ($request->ajax()) {
            return response()->json(['page' => $view]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Costumer  $costumer
     * @return \Illuminate\Http\Response
     */
    public function edit(Costumer $costumer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\CostumerUpdate  $request
     * @param  \App\Models\Costumer  $costumer
     * @return \Illuminate\Http\Response
     */
    public function update(CostumerUpdate $request, Costumer $costumer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Costumer  $costumer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Costumer $costumer)
    {
        $costumer->delete();
    }
}
