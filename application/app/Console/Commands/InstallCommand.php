<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'khrisnaTest:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command make it easier installation for the user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("Building By Khrisna For Test On Stand Studio !");
        $this->info("For More Info khrisna7arsita@gmail.com || C 2020");
        if ($this->confirm('Pastikan Configurasi .env anda benar dan kami akan membuat structure database untuk anda ?')) {
            
            $bar = $this->output->createProgressBar(11);

            $bar->start();

            $this->call('migrate:fresh',['--seed' => true,]);

            $bar->finish();
        }
    }
}
