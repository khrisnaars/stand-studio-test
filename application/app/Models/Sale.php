<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $table = 'costumer_product';

    protected $fillable = [
        'price'
    ];

    public $timestamps = true;
}
