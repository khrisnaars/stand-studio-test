<!DOCTYPE html>

<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 7
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>STAND-STUDIO TEST</title>
		<meta name="description" content="Updates and statistics">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>

		<!--end::Fonts -->

		<!--begin::Page Vendors Styles(used by this page) -->
		<link href="{{assets('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{assets('assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{assets('assets/morris-charts/morris.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{assets('assets/leaflet/leaflet.css')}}" rel="stylesheet" type="text/css" />
		
		<!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Page Vendors Styles -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="{{assets('assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{assets('assets/line-awesome/css/line-awesome.css')}}" rel="stylesheet" type="text/css" />
		<!--RTL version:<link href="assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
		<link href="{{assets('assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />

		<!--RTL version:<link href="assets/demo/default/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->

		<!--RTL version:<link href="assets/demo/default/skins/header/base/light.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--RTL version:<link href="assets/demo/default/skins/header/menu/light.rtl.css" rel="stylesheet" type="text/css" />-->
		<link href="{{assets('assets/demo/default/skins/brand/dark.css')}}" rel="stylesheet" type="text/css" />

		<!--RTL version:<link href="assets/demo/default/skins/brand/dark.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--RTL version:<link href="assets/demo/default/skins/aside/dark.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Layout Skins -->
		{{-- <link rel="shortcut icon" href="{{assets('assets/media/logos/favicon.ico')}}" /> --}}
		
		@yield('customStyle')
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="offset-lg-1 col-lg-10  col-12 kt-header--fixed kt-header-mobile--fixed kt-page--loading">
<!-- begin:: Page -->

		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
					@yield('dashboard')
				
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
						<!-- begin:: Content Head -->
						<div class="kt-subheader  kt-grid__item" id="kt_subheader">
							<div class="col-xl-12 col-12">
							<div class="kt-subheader__main" style="padding-top: 3%;padding-bottom: 3%">
								<span class="kt-subheader__separator kt-subheader__separator--v"></span>
								<h3 class="kt-subheader__title" style="font-size: 200%;">STAND-STUDIO TEST</h3>
							</div>
							</div>
						</div>

						<!-- end:: Content Head -->

						<!-- begin:: Content -->
						@yield('content')
						<!-- end:: Content -->
					</div>
				</div>
					<!-- begin:: Footer -->
					<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop">
						<div class="kt-footer__copyright">
							2019&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
						</div>
					</div>

					<!-- end:: Footer -->
				
			</div>

		<!-- end:: Page -->

		<!-- end::Quick Panel -->

		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>

		<!-- end::Scrolltop -->

		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>

		<!-- end::Global Config -->
		
		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="{{assets('assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
		<script src="{{assets('assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors(used by this page) -->
		<script src="{{assets('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
		<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script>
		<script src="{{assets('assets/vendors/custom/gmaps/gmaps.js')}}" type="text/javascript"></script>
		<script src="{{assets('assets/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
		<script src="{{assets('assets/morris-charts/morris.js')}}" type="text/javascript"></script>
		<script src="{{assets('assets/leaflet/leaflet.js')}}" type="text/javascript"></script>
		<!--end::Page Vendors -->

		<!-- jQuery UI 1.11.4 -->
		<script src="{{assets('vendors/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
		
		<!--begin::Page Scripts(used by this page) -->
		<script src="{{assets('assets/app/custom/general/dashboard.js')}}" type="text/javascript"></script>
		<script src="{{assets('js/moment.js')}}" type="text/javascript"></script>

		@yield('customScript')
		<!--end::Page Scripts -->

		<!--begin::Global App Bundle(used by all pages) -->
		<script src="{{assets('assets/app/bundle/app.bundle.js')}}" type="text/javascript"></script>

		<!--end::Global App Bundle -->

		
	</body>

	<!-- end::Body -->
</html>