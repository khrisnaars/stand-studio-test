@extends('admin.dashboard')

@section('customStyle')
<style>
.modal-body {
    max-height: calc(100vh - 210px);
    overflow-y: auto;
}
</style>
@endsection

@section('content')
<div style="margin-top:2%" class="kt-portlet">
    <div class="kt-portlet__head bg-warning" style="justify-content: center">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title" style="font-size: 200%">
                TEST STAND KHRISNA
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">

        @if($errors->any())
            {!! implode('', $errors->all('<div class="alert alert-danger">:message</div>')) !!}
        @endif
        
        @if (\Session::has('success'))
            <div class="alert alert-success">
                {!! Session::get('success') !!}
            </div>
        @endif

        <form class="kt-form" method="POST" action="{{route('costumer.store')}}">
            @csrf
            <div class="kt-portlet__body">
                <div class="kt-section kt-section--first">
                    <div class="form-group">
                        <label>Company :</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter company name" required>
                        <span class="form-text text-muted">Costumer company name...</span>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </form>

        {{$costumer->links()}}
        <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
            <thead>
                <th>
                    Company
                </th>
                <th>
                    Aksi
                </th>
            </thead>
            <tbody>
                @foreach ($costumer as $item)
                    <tr>
                        <td>
                            {{$item->name}}
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger" onclick="deleteData({{$item->id}})">Delete</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('customScript')
<script type="text/javascript">
    $(document).ready(function() {
        let table = $('#kt_table_1').DataTable();
    } );

    function deleteData(id){
        $.ajax({
        url: "{{url('costumer')}}"+'/'+id,
        type: 'delete',
        data: {_token: $("input[name=_token]").val()},
        success: function(response){
            window.location.reload();
        }
      });
    }
</script>
@endsection