@extends('admin.dashboard')

@section('customStyle')
<style>
.modal-body {
    max-height: calc(100vh - 210px);
    overflow-y: auto;
}
</style>
@endsection

@section('content')
<div style="margin-top:2%" class="kt-portlet">
    <div class="kt-portlet__head bg-warning" style="justify-content: center">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title" style="font-size: 200%">
                TEST STAND KHRISNA
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        
        {{$costumer->links()}}
        <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
            <thead>
                <th>
                    Company
                </th>
                <th>
                    Aksi
                </th>
            </thead>
            <tbody>
                @foreach ($costumer as $item)
                    <tr>
                        <td>
                            {{$item->name}}
                        </td>
                        <td>
                            <button type="button" data-toggle="modal" onclick="getData({{$item->id}},1)" data-target="#kt_modal_1" class="btn btn-warning detailProduct">Products</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="modal fade" id="kt_modal_1" data-id="" data-page="1" tabindex="-1" data-page="1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                    </div>
                    <div class="modal-body">
                        <button onclick="backPage()">back</button>
                        <button onclick="nextPage()">next</button>
                        <div id="table-content"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
</div>
@endsection

@section('customScript')
   <script type="text/javascript">
        $(document).ready(function() {
            let table = $('#kt_table_1').DataTable(); 
        });

        function getData(id,page){
            $('#kt_modal_1').data('id', id);

            $('#exampleModalLabel').html($(this).data('name'));

            $.getJSON("{{url('costumer')}}"+"/"+id+"?page="+page, function(result){
                if(result.page != ""){
                    $('#table-content').html(result.page);
                    $('#kt_modal_1').data('page',page);
                }else{
                    alert('Last Page');
                }
            });
        }

        function backPage(){
            var currentId = $('#kt_modal_1').data('id');
            var currentPage = $('#kt_modal_1').data('page');

            var backPage = currentPage-1;
            if(backPage != 0){;
                getData(currentId,backPage);
            }
        }

        function nextPage() {
            var currentId = $('#kt_modal_1').data('id');
            var currentPage = $('#kt_modal_1').data('page');

            var nextPage = currentPage+1;

            if(nextPage != 0){
                getData(currentId,nextPage);
            }
        }
    </script>
@endsection