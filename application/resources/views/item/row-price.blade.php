
@if (count($costumer) != 0)
<table style="width: 100%" class="table table-striped- table-bordered table-hover table-checkable">
    <thead>
        <th>Product</th>
        <th>Price</th>
    </thead>
    <tbody>
        @foreach ($costumer as $data)
            <tr>
                <td>{{$data->name}}</td>
                <td>{{$data->pivot->price}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
@endif