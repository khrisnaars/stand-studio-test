<?php

use App\Models\Costumer;
use App\Models\Product;
use Illuminate\Database\Seeder;
use App\Models\Sale;

use Faker\Factory as Faker;

class SalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(100000, 500000) as $index) {
            Sale::insert([
                'costumer_id' => Costumer::all()->random()->id,
                'product_id' => Product::all()->random()->id,
                'price' => $faker->numberBetween(1000,1000000)
            ]);
        }
    }
}
