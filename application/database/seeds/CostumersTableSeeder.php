<?php

use Illuminate\Database\Seeder;
use App\Models\Costumer;

use Faker\Factory as Faker;

class CostumersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(500, 5000) as $index) {
            Costumer::insert([
                'name' => $faker->company
            ]);
        }
    }
}
