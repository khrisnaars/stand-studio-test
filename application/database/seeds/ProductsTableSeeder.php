<?php

use Illuminate\Database\Seeder;
use App\Models\Product;

use Faker\Factory as Faker;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(500, 5000) as $index) {
            Product::insert([
                'name' => $faker->country
            ]);
        }
    }
}
